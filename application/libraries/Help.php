<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Help{
	public function __construct(){
      $this->ci =& get_instance();
    }

	public function labelnya(){
		return 'Isian dengan tanda (<span style="color:red">*</span>) tidak boleh kosong';
	}

	public function MoneyToDouble($num) {
        if (substr($num, 0, 1) == '(') {
            $num = '-'.preg_replace('/[\(\)]/', '', $num);
        }

        return $num = (double) str_replace(',', '', $num);
    }

    public function ReverseTgl($tgl) {
        $tgl = explode('-', $tgl);
        if (count($tgl) != 3)
            return $tgl[0];
        $tmp = '';
        for ($i = count($tgl) - 1; $i >= 0; $i--) {
            $tmp .= $tgl[$i] . '-';
        }
        $tmp = substr($tmp, 0, strlen($tmp) - 1);
        $tgl = $tmp;
        //$tgl[2] . '-' . $tgl[1] . '-' . $tgl[0];
        return $tgl;
    }

    public function namaBulan($x=null) { 
        $bulan = array(
                    '01'=>'Januari',
                    '02'=>'Februari',
                    '03'=>'Maret',
                    '04'=>'April',
                    '05'=>'Mei',
                    '06'=>'Juni',
                    '07'=>'Juli',
                    '08'=>'Agustus',
                    '09'=>'September',
                    '10'=>'Oktober',
                    '11'=>'November',
                    '12'=>'Desember'
                );
        if($x){
            return $bulan[$x];
        }
        
        return $bulan;
    }

    public function mkdir_nya($target_dir){
        if((!file_exists($target_dir))&&(!is_dir($target_dir))){
            mkdir('./'.$target_dir,0777,true);
            $indx=fopen($target_dir.'/'.'index.php','w');
            fwrite($indx,'<!DOCTYPE html>
                        <html>
                        <head>
                            <title>403 Forbidden</title>
                        </head>
                        <body>
                        <p>Directory access is forbidden.</p>
                        </body>
                        </html>');
            fclose($indx);
        }
    }
}