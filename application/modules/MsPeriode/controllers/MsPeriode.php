<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MsPeriode extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('MMsPeriode');
	}

	public function index(){
		$data=null;
		$data['periode'] = 'active';
		$data['act_add'] = base_url().'MsPeriode/create';
		$this->template->load('Home/template','MsPeriode/list',$data);
	}

	public function getDatatables(){
		header('Content-Type: application/json');
        
        $this->datatables->select('id,nama_periode,status');
        $this->datatables->from("ms_periode");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('MsPeriode/update/$1'),'<i title="edit" class="glyphicon glyphicon-edit icon-white"></i>','class="btn btn-xs btn-success"').anchor(site_url('MsPeriode/delete/$1'),'<i title="hapus" class="glyphicon glyphicon-trash icon-white"></i>','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"').'</div>', 'id');

        echo $this->datatables->generate();
	}

	public function create(){
		$data = array(
			'action' => base_url().'MsPeriode/save',
			'button' => 'Simpan',
			'nama_periode' => set_value('nama_periode'),
			'status' => set_value('status'),
			'id' => set_value('id'),
		);
		
		$data['periode'] = 'active';
		$this->template->load('Home/template','MsPeriode/form',$data);
	}

	public function update($id){
		$kat = $this->MMsPeriode->get(array('id'=>$id));
		$kat = $kat[0];

		$data = array(
			'action' => base_url().'MsPeriode/save',
			'button' => 'Update',
			'nama_periode' => set_value('nama_periode',$kat['nama_periode']),
			'status' => set_value('status',$kat['status']),
			'id' => set_value('id',$kat['id']),
		);

		$data['periode'] = 'active';
		$this->template->load('Home/template','MsPeriode/form',$data);
	}

	public function save(){
		$id = $this->input->post('id');
		$data['nama_periode'] = $this->input->post('nama_periode');
		$data['status'] = $this->input->post('status');

		if(empty($id)){
			$this->MMsPeriode->insert($data);
			$this->session->set_flashdata('success', 'Data Berhasil disimpan.');
		
		}else{
			$this->MMsPeriode->update($id,$data);
			$this->session->set_flashdata('success', 'Data Berhasil diupdate.');
		}
		
        redirect('MsPeriode');
	}

	public function delete($id){       
        $result = $this->MMsPeriode->delete($id);
		if($result){
        	$this->session->set_flashdata('success', 'Data berhasil dihapus.');
        }else{
        	$this->session->set_flashdata('error', 'Data hanya bisa diupdate');
        }

        redirect('MsPeriode');
	}
}
