          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>User</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <ul class="nav navbar-right panel_toolbox">
                      <li class="invisible">
                        <a>&nbsp;</a>
                      </li>
                      <li class="invisible">
                        <a>&nbsp;</a>
                      </li>
                      <li class="invisible">
                        <a>&nbsp;</a>
                      </li>
                      <li>
                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="<?= $action; ?>" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Username
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="username" name="username" required class="form-control col-md-7 col-xs-12" value="<?=$username?>">
                        </div>
                      </div>
                      <?php if(empty($id)):?>
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Password
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="password" name="password" required class="form-control col-md-7 col-xs-12" value="<?=$password?>">
                        </div>
                      </div>
                      <?php endif; ?>
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Lengkap
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nama_lengkap" name="nama_lengkap" required class="form-control col-md-7 col-xs-12" value="<?=$nama_lengkap?>">
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Level</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" tabindex="-1" id="level" name="level" required>
                            <option <?php echo $level==1?'selected':''?> value="1">Administrator</option>
                            <!-- <option <?php echo $level==2?'selected':''?> value="2">Anggota</option> -->
                            <!-- <option <?php echo $level==3?'selected':''?> value="3">SKPD</option> -->
                            <option <?php echo $level==4?'selected':''?> value="4">User Persidangan</option>
                            <option <?php echo $level==5?'selected':''?> value="5">User Perundangan</option>
                            <option <?php echo $level==6?'selected':''?> value="6">User Keuangan</option>
                            <option <?php echo $level==7?'selected':''?> value="7">User Persuratan</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Blokir
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <label class="radio-inline">
                            <input id="inlineRadio1" name="blokir" value="Y" type="radio" <?php echo $blokir=='Y'?'checked':'';?> >
                                        Ya
                          </label>
                          <label class="radio-inline">
                              <input id="inlineRadio2" name="blokir" value="N" type="radio" <?php echo $blokir!='Y'?'checked':'';?> >
                                        Tidak
                          </label>
                        </div>
                      </div>
                      <?php if($id):?>
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Reset Password
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div class="input-group">
                              <div class="col-md-1">
                                  <input name="reset_password" id="reset_password" type="checkbox">
                                  <span id="info_pswd" class="label-success label label-default">Password awal : 123456</span>
                              </div>
                          </div>
                        </div>
                      </div>
                      <?php endif;?>
                      <input type="hidden" name="id" value="<?=$id?>">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Batal</button>
                          <button type="submit" class="btn btn-success"><?= $button?></button>
                      </div>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
<script type="text/javascript">
$(document).ready(function(){
    $('#info_pswd').hide();
    $("#reset_password").click(function(){
        if ($('#reset_password').is(':checked')) {
            $('#info_pswd').show();
        }else{
            $('#info_pswd').hide();
        }
    });
});
</script>