<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#data_diri" data-toggle="tab">Data Diri</a></li>
        <li><a href="#riwayat_pendidikan" data-toggle="tab">Riwayat Pendidikan</a></li>
        <li><a href="#riwayat_pekerjaan" data-toggle="tab">Riwayat Pekerjaan</a></li>
        <li><a href="#riwayat_jabatan" data-toggle="tab">Riwayat Jabatan</a></li>
        <li><a href="#riwayat_kesehatan" data-toggle="tab">Riwayat Kesehatan</a></li>
        <li><a href="#data_keluarga" data-toggle="tab">Data Keluarga</a></li>
        <li><a href="#riwayat_penugasan" data-toggle="tab">Riwayat Penugasan</a></li>
        <li><a href="#kegiatanku" data-toggle="tab">Kegiatanku Di DPRD</a></li>
    </ul>
    <div class="tab-content">
        <br>                    
        <div class="tab-pane active" id="data_diri">
            <div class="row">
                <div class="col-md-2 col-lg-2 " align="center"> <img alt="User Pic" src="<?=base_url().$anggota->foto_upload?>" class="img-thumbnail img-responsive"> </div>
                <div class=" col-md-9 col-lg-9">
                    <div class="table-responsive">   
                    <table class="table table-user-information">
                        <tbody>
                            <tr>
                                <td>No Urut</td>
                                <td width="2">:</td>
                                <td><?=$anggota->no_urut_anggota?></td>
                                <td>Asal Parpol</td>
                                <td width="2">:</td>
                                <td><?=$anggota->asal_parpol?></td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td><?=$anggota->nama?></td>
                                <td>Asal Dapil</td>
                                <td>:</td>
                                <td><?=$anggota->asal_dapil?></td>
                            </tr> 
                            <tr>
                                <td>Tempat Lahir</td>
                                <td>:</td>
                                <td><?=$anggota->tempat_lahir?></td>
                                <td>Asal Fraksi</td>
                                <td>:</td>
                                <td><?=$anggota->asal_fraksi?></td>
                            </tr>  
                            <tr>
                                <td>Tgl Lahir</td>
                                <td>:</td>
                                <td><?=$anggota->tgl_lhrnya?></td>
                                <td>Perolehan Suara</td>
                                <td>:</td>
                                <td><?=$anggota->hasil_suara?></td>
                            </tr>
                            <tr>
                                <td>Jenis Kelamin</td>
                                <td>:</td>
                                <td><?=$anggota->jk?></td>
                                <td>Periode</td>
                                <td>:</td>
                                <td><?=$anggota->nama_periode?></td>
                            </tr>
                            <tr>
                                <td>Agama</td>
                                <td>:</td>
                                <td><?=$anggota->agama?></td>
                                <td>No Telp.</td>
                                <td>:</td>
                                <td><?=$anggota->no_tlp?></td>
                            </tr>
                            <tr>
                                <td>Email/Username</td>
                                <td>:</td>
                                <td><?=$anggota->email?></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>:</td>
                                <td colspan="4"><?=$anggota->alamat?></td>
                            </tr>                                 
                        </tbody>
                    </table>
                    </div> 
                </div>
            </div>
        </div>
        <div class="tab-pane" id="riwayat_pendidikan">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="table-responsive">                     
                        <table class="table table-bordered table-striped example_data">
                            <thead>
                                <tr>
                                    <th style="text-align: center" width="40px">No</th>
                                    <th style="text-align: center">Tingkat</th>
                                    <th style="text-align: center">Nama Pendidikan</th>
                                    <th style="text-align: center">Jurusan</th>
                                    <th style="text-align: center">Tahun Lulus</th>
                                    <th style="text-align: center">Tempat Pendidikan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1;?>
                                <?php foreach((array)$arrPendidikan as $valPdk) :?>
                                    <tr>
                                        <td class="text-center"><?php echo $no++;?></td>
                                        <td class="text-center"><?=$valPdk['tingkat']?></td>
                                        <td class="text-center"><?=$valPdk['nama_pddk']?></td>
                                        <td class="text-center"><?=$valPdk['jurusan']?></td>
                                        <td class="text-center"><?=$valPdk['tahun_lulus']?></td>
                                        <td class="text-center"><?=$valPdk['tempat_pddk']?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="riwayat_pekerjaan">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="table-responsive"> 
                        <table class="table table-bordered table-striped example_data">
                            <thead>
                                <tr>
                                    <th style="text-align: center" width="40px">No</th>
                                    <th style="text-align: center">Jenis Pekerjaan</th>
                                    <th style="text-align: center">Nama Instansi</th>
                                    <th style="text-align: center">Lama Bekerja</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1;?>
                                <?php foreach((array)$arrPekerjaan as $valPkr) :?>
                                    <tr>
                                        <td class="text-center"><?php echo $no++;?></td>
                                        <td class="text-center"><?=$valPkr['jenis_pekerjaan']?></td>
                                        <td class="text-center"><?=$valPkr['nama_instansi']?></td>
                                        <td class="text-center"><?=$valPkr['lama_bekerja']?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="riwayat_jabatan">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="table-responsive"> 
                        <table class="table table-bordered table-striped example_data">
                            <thead>
                                <tr>
                                    <th style="text-align: center" width="40px">No</th>
                                    <th style="text-align: center">Jabatan</th>
                                    <th style="text-align: center">Tgl Mulai</th>
                                    <th style="text-align: center">Tgl Selesai</th>
                                    <th style="text-align: center">Keterangan</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1;?>
                                <?php foreach((array)$arrJabatan as $valJbt) :?>
                                    <tr>
                                        <td class="text-center"><?php echo $no++;?></td>
                                        <td class="text-center"><?=$valJbt['rw_nama_jabatan']?></td>
                                        <td class="text-center"><?=$valJbt['tgl_mulai']?></td>
                                        <td class="text-center"><?=$valJbt['tgl_selesai']?></td>
                                        <td class="text-center"><?=$valJbt['keterangan_jabatan']?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="riwayat_kesehatan">
            <div class="col-md-2 col-lg-2 " align="center"> <img alt="User Pic" src="<?=base_url().$anggota->id_bpjs?>" class="img-thumbnail img-responsive"> </div>
            <div class=" col-md-9 col-lg-9">
                <div class="table-responsive"> 
                    <table class="table table-bordered table-striped example_data">
                        <thead>
                            <tr>
                                <th style="text-align: center" width="40px">No</th>
                                <th style="text-align: center">Jenis Sakit</th>
                                <th style="text-align: center">Tgl Sakit</th>
                                <th style="text-align: center">Tempat Perawatan</th>
                                <th style="text-align: center">Lama Perawatan</th>
                                <th style="text-align: center">Kondisi</th>
                                <th style="text-align: center">Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no=1;?>
                            <?php foreach((array)$arrKesehatan as $valKes) :?>
                                <tr>
                                    <td class="text-center"><?php echo $no++;?></td>
                                    <td class="text-center"><?=$valKes['jenis_sakit']?></td>
                                    <td class="text-center"><?=$this->help->ReverseTgl($valKes['tgl_sakit'])?></td>
                                    <td class="text-center"><?=$valKes['tempat_perawatan']?></td>
                                    <td class="text-center"><?=$valKes['lama_perawatan']?></td>
                                    <td class="text-center"><?=$valKes['kondisi']?></td>
                                    <td class="text-center"><?=$valKes['keterangan']?></td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="data_keluarga">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-10">
                    <div class="table-responsive">                     
                        <table class="table table-bordered table-striped example_data">
                            <thead>
                                <tr>
                                    <th style="text-align: center" width="40px">No</th>
                                    <th style="text-align: center">Nama Suami/Istri/Anak</th>
                                    <th style="text-align: center">NIK</th>
                                    <th style="text-align: center">Hubungan</th>
                                    <th style="text-align: center">Tempat Lahir</th>
                                    <th style="text-align: center">Tgl Lahir</th>
                                    <!-- <th style="text-align: center">Pendidikan</th> -->
                                    <th style="text-align: center">Status Anak</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no=1;?>
                                <?php foreach((array)$arrKeluarga as $valHub) :?>
                                    <tr>
                                        <td class="text-center"><?php echo $no++;?></td>
                                        <td class="text-center"><?=$valHub['nama_keluarga']?></td>
                                        <td class="text-center"><?=$valHub['nik_keluarga']?></td>
                                        <td class="text-center"><?=$valHub['hubungan']?></td>
                                        <td class="text-center"><?=$valHub['tempat_lahir_kel']?></td>
                                        <td class="text-center"><?=$this->help->ReverseTgl($valHub['tgl_lahir_kel'])?></td>
                                        <!-- <td class="text-center"><?=$valHub['pendidikan']?></td> -->
                                        <td class="text-center"><?=$valHub['status_anak']?></td>
                                    </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php 
            $urlRiwayatPenugasan=base_url().'View/Riwayat';
            $urlKegiatanku=base_url().'View/Kegiatanku';
            if ($this->session->level==1) {
                $urlRiwayatPenugasan.='/'.$idAnggota;
                $urlKegiatanku.='/'.$idAnggota;
            } ?>
        <div class="tab-pane" id="riwayat_penugasan">
            <div class="row col-md-12">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <a data-toggle="tooltip" title="Riwayat Penugasan" class="well top-block" href="<?=$urlRiwayatPenugasan?>">Riwayat Penugasan</a>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="kegiatanku">
            <div class="row col-md-12">
                <div class="col-md-3 col-sm-3 col-xs-6">
                    <a data-toggle="tooltip" title="Kegiatanku di DPRD" class="well top-block" href="<?=$urlKegiatanku?>">Kegiatanku</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
    $('.example_data').dataTable({
        "searching": false,
        "bLengthChange": false,
        "bSort": false,
        'oLanguage': {
            "sProcessing":   "Sedang memproses...",
            "sLengthMenu":   "Tampilkan _MENU_ entri",
            "sZeroRecords":  "Tidak ditemukan data yang sesuai",
            "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix":  "",
            "sSearch":       "Cari:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "<<",
                "sPrevious": "<",
                "sNext":     ">",
                "sLast":     ">>"
            }
        },
    });
});
</script>