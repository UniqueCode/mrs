<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends MX_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->model('MMsAnggota');
		// $this->load->model('MMsAnggotaKeluarga');
		// $this->load->model('MMsAnggotaPendidikan');
		// $this->load->model('MMsAnggotaPekerjaan');
		// $this->load->model('MMsAnggotaJabatan');
		// $this->load->model('MMsAnggotaKesehatan');
		// $this->load->model('MPenghasilanBulanan');
		// $this->load->model('MPenerimaanDOP');
		// $this->load->model('MPenerimaanLain');
		// $this->load->model('MPengisianSpt');
		// $this->load->model('MPengisianLhkpn');
		// $this->load->model('MHakKeuangan');
		// $this->load->model('MRenja');
		// $this->load->model('MJadwalkegiatan');

		// $this->load->model('MKedudukanHukumDprd');
		// $this->load->model('MProfilAPBD');
		// $this->load->model('MProfilDPRD');

		
	}

	public function index($id=null){
		$id_anggota = $this->session->fk_anggota_id;
		if($id){
			$id_anggota = $id;
		}
		// $dataAnggota = $this->db->query("select count(id) jml from v_ms_anggota where status_periode=1")->row();
		// $data_diri = 0;
		// if($this->session->level==2 || !empty($id)){ //anggota dewan
		// 	$data_diri = 1;
		// 	$penghasilan = $this->MPenghasilanBulanan->get(array('fk_anggota_id'=>$id_anggota,'is_read'=>'0'));
		// 	$trmaDOP = $this->MPenerimaanDOP->get(array('fk_anggota_id'=>$id_anggota,'is_read'=>'0'));
		// 	$trmaLain = $this->MPenerimaanLain->get(array('fk_anggota_id'=>$id_anggota,'is_read'=>'0'));
		// 	$isiSPT = $this->MPengisianSpt->get(array('fk_anggota_id'=>$id_anggota,'is_read'=>'0'));
		// 	$isiLhkpn = $this->MPengisianLhkpn->get(array('fk_anggota_id'=>$id_anggota,'is_read'=>'0'));
		// 	$data['penghasilanBulanan']=count($penghasilan);
		// 	$data['penerimaanDOP']=count($trmaDOP);
		// 	$data['cekDOP']=$this->MPenerimaanDOP->get(array('fk_anggota_id'=>$id_anggota));
		// 	$data['penerimaanLain']=count($trmaLain);
		// 	$data['pengisianSPT']=count($isiSPT);
		// 	$data['pengisianLHKPN']=count($isiLhkpn);

		// 	$thnSblm = date('Y', strtotime('-1 year', strtotime(date('Y'))));
		// 	$data['cekSPT'] = $this->MPengisianSpt->get(array('fk_anggota_id'=>$id_anggota,'tahun'=>$thnSblm));
		// 	$data['cekLHKPN'] = $this->MPengisianLhkpn->get(array('fk_anggota_id'=>$id_anggota,'tahun'=>$thnSblm));
		// 	$data['dasarBesaran'] = $this->MHakKeuangan->get();
			
		// 	//Informasi Hak dan Kewajiban Administratif
		// 	$data['kehadiranRapat'] = $this->db->query("SELECT r.nama_rapat, r.nama_sub_rapat, k.waktu_pelaksanaan, k.perihal_rapat, t.keterangan,k.file_upload FROM v_kehadiran_rapat k JOIN ms_rapat r ON k.fk_ms_rapat = r.id JOIN t_kehadiran_rapat_detail t ON t.fk_kehadiran_rapat = k.id JOIN ms_anggota a ON t.fk_anggota_id = a.id WHERE t.fk_anggota_id = '$id_anggota' ORDER BY tahun_dari DESC, bulan_dari DESC, tgl_dari DESC")->result();
		// 	$data['ijindanCuti'] = $this->db->query("SELECT i.id, a.nama, i.kategori, date_format( i.tgl_mulai, '%d-%m-%Y' ) tgl_mulai, date_format( i.tgl_berakhir, '%d-%m-%Y' ) tgl_berakhir, i.keterangan FROM t_ijin_cuti i JOIN ms_anggota a ON i.fk_anggota_id = a.id JOIN ms_periode p ON p.id = a.fk_periode_id WHERE p.STATUS = '1' AND i.fk_anggota_id = '$id_anggota' ORDER BY i.id ASC")->result();
		// 	$data['generalCheckup'] = $this->db->query("SELECT g.tahun,d.status,d.tgl_checkup FROM t_general_checkup g JOIN t_general_checkup_detail d ON g.id = d.fk_general_checkup JOIN ms_anggota a ON d.fk_anggota_id = a.id WHERE d.fk_anggota_id = '$id_anggota' ORDER BY g.tahun DESC")->row();

		// 	$anggt=$this->db->query("select fk_periode_id from ms_anggota where id=$id_anggota")->row();
		// 	$data['arrRenja'] = $this->MRenja->get(array('fk_periode_id'=>$anggt->fk_periode_id));
		// 	$data['arrJdwlPersidangan'] = $this->MJadwalkegiatan->get(array('fk_periode_id'=>$anggt->fk_periode_id));
		// 	$data['hasilRapat'] = $this->db->query("SELECT t.* FROM t_hasil_rapat t JOIN t_hasil_rapat_detail td ON td.fk_hasil_rapat_id = t.id WHERE td.fk_anggota_id = '$id_anggota' ORDER BY tgl DESC")->result_array();
		// 	$data['kegReses'] = $this->db->query("SELECT t.* FROM t_kegiatan_reses t JOIN t_kegiatan_reses_detail td ON td.fk_kegiatan_reses_id = t.id WHERE td.fk_anggota_id = '$id_anggota' ORDER BY tahun,tgl DESC")->result_array();
		// 	$data['lapReses'] = $this->db->query("SELECT t.* FROM t_laporan_reses t JOIN t_laporan_reses_detail td ON td.fk_laporan_reses_id = t.id WHERE td.fk_anggota_id = '$id_anggota' ORDER BY tahun DESC")->result_array();
		// 	//Dukungan n Fungsi
		// 	$data['arrKedudukanHukum'] = $this->MKedudukanHukumDprd->get();
		// 	$data['arrProfilAPBD'] = $this->MProfilAPBD->get();
		// 	$data['arrProfilDprd'] = $this->MProfilDPRD->get(array('kategori'=>'DPRD'));
		// 	$data['arrProfilKabupaten'] = $this->MProfilDPRD->get(array('kategori'=>'KABUPATEN'));

		// 	$data['jmlSurat']=$this->db->query("select count(fk_surat_id) jml from t_surat_anggota_detail where fk_anggota_id=$id_anggota AND is_read='0'")->row()->jml;
		// 	$data['idAnggota']=$id_anggota;
		// }

		// $data['jml_anggota']=$dataAnggota->jml;
		// $data['data_diri']=$data_diri;

		$data['beranda'] = 'active';		
		$this->template->load('Home/template','Home/beranda', $data);
	}

	public function viewDataDiri(){
		$id=$this->input->post('id');
		$anggota = $this->db->query("select * from v_ms_anggota WHERE id=".$id."")->row();

		$dataAng['anggota'] = $anggota;
		$dataAng['arrKeluarga'] = $this->MMsAnggotaKeluarga->get(array('fk_anggota_id'=>$id));
		$dataAng['arrPendidikan'] = $this->MMsAnggotaPendidikan->get(array('fk_anggota_id'=>$id));
		$dataAng['arrPekerjaan'] = $this->MMsAnggotaPekerjaan->get(array('fk_anggota_id'=>$id));
		$dataAng['arrJabatan'] = $this->MMsAnggotaJabatan->get(array('fk_anggota_id'=>$id));
		$dataAng['arrKesehatan'] = $this->MMsAnggotaKesehatan->get(array('fk_anggota_id'=>$id));
		$dataAng['idAnggota']=$id;
		$this->load->view('Home/biodata_anggota',$dataAng);
	}
}
