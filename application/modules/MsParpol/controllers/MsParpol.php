<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MsParpol extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('MMsPeriode');
		$this->load->model('MMsParpol');
	}

	public function index(){
		$data=null;
		$data['parpol'] = 'active';
		$data['act_add'] = base_url().'MsParpol/create';
		$this->template->load('Home/template','MsParpol/list',$data);
	}

	public function getDatatables(){
		header('Content-Type: application/json');
        
        $this->datatables->select('id,nama_parpol,nama_periode,status');
        $this->datatables->from("v_ms_parpol");
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('MsParpol/update/$1'),'<i title="edit" class="glyphicon glyphicon-edit icon-white"></i>','class="btn btn-xs btn-success"').anchor(site_url('MsParpol/delete/$1'),'<i title="hapus" class="glyphicon glyphicon-trash icon-white"></i>','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"').'</div>', 'id');

        echo $this->datatables->generate();
	}

	public function create(){
		$data = array(
			'action' => base_url().'MsParpol/save',
			'button' => 'Simpan',
			'fk_periode_id' => set_value('fk_periode_id'),
			'nama_parpol' => set_value('nama_parpol'),
			'id' => set_value('id'),
		);
		$data['parpol'] = 'active';
		$data['arrPeriode'] = $this->MMsPeriode->get(array('status'=>1));
		$this->template->load('Home/template','MsParpol/form',$data);
	}

	public function update($id){
		$kat = $this->MMsParpol->get(array('id'=>$id));
		$kat = $kat[0];

		$data = array(
			'action' => base_url().'MsParpol/save',
			'button' => 'Update',
			'fk_periode_id' => set_value('fk_periode_id',$kat['fk_periode_id']),
			'nama_parpol' => set_value('nama_parpol',$kat['nama_parpol']),
			'id' => set_value('id',$kat['id']),
		);
		$data['parpol'] = 'active';
		$data['arrPeriode'] = $this->MMsPeriode->get(array('status'=>1));
		$this->template->load('Home/template','MsParpol/form',$data);
	}

	public function save(){
		$id = $this->input->post('id');
		$data['fk_periode_id'] = $this->input->post('fk_periode_id');
		$data['nama_parpol'] = $this->input->post('nama_parpol');

		if(empty($id)){
			$this->MMsParpol->insert($data);
			$this->session->set_flashdata('success', 'Data Berhasil disimpan.');
		
		}else{
			$this->MMsParpol->update($id,$data);
			$this->session->set_flashdata('success', 'Data Berhasil diupdate.');
		}
		
        redirect('MsParpol');
	}

	public function delete($id){       
        $result = $this->MMsParpol->delete($id);
		if($result){
        	$this->session->set_flashdata('success', 'Data berhasil dihapus.');
        }else{
        	$this->session->set_flashdata('error', 'Data hanya bisa diupdate');
        }

        redirect('MsParpol');
	}
}
