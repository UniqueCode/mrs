<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MsJabatan extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('MMsJabatan');
	}

	public function index(){
		$data=null;
		$data['jabatan'] = 'active';
		$data['act_add'] = base_url().'MsJabatan/create';
		$this->template->load('Home/template','MsJabatan/list',$data);
	}

	public function getDatatables(){
		header('Content-Type: application/json');
        
        $this->datatables->select('id,kode,nama_jabatan');
        $this->datatables->from("ms_jabatan");
        $this->db->order_by('cast(kode AS UNSIGNED INTEGER)','asc');
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('MsJabatan/update/$1'),'<i title="edit" class="glyphicon glyphicon-edit icon-white"></i>','class="btn btn-xs btn-success"').anchor(site_url('MsJabatan/delete/$1'),'<i title="hapus" class="glyphicon glyphicon-trash icon-white"></i>','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"').'</div>', 'id');

        echo $this->datatables->generate();
	}

	public function create(){
		$data = array(
			'action' => base_url().'MsJabatan/save',
			'button' => 'Simpan',
			'kode' => set_value('kode'),
			'nama_jabatan' => set_value('nama_jabatan'),
			'id' => set_value('id'),
		);
		$data['jabatan'] = 'active';
		$this->template->load('Home/template','MsJabatan/form',$data);
	}

	public function update($id){
		$kat = $this->MMsJabatan->get(array('id'=>$id));
		$kat = $kat[0];

		$data = array(
			'action' => base_url().'MsJabatan/save',
			'button' => 'Update',
			'kode' => set_value('kode',$kat['kode']),
			'nama_jabatan' => set_value('nama_jabatan',$kat['nama_jabatan']),
			'id' => set_value('id',$kat['id']),
		);
		$data['jabatan'] = 'active';
		$this->template->load('Home/template','MsJabatan/form',$data);
	}

	public function save(){
		$id = $this->input->post('id');
		$data['kode'] = $this->input->post('kode');
		$data['nama_jabatan'] = $this->input->post('nama_jabatan');

		if(empty($id)){
			$this->MMsJabatan->insert($data);
			$this->session->set_flashdata('success', 'Data Berhasil disimpan.');
		
		}else{
			$this->MMsJabatan->update($id,$data);
			$this->session->set_flashdata('success', 'Data Berhasil diupdate.');
		}
		
        redirect('MsJabatan');
	}

	public function delete($id){       
        $result = $this->MMsJabatan->delete($id);
		if($result){
        	$this->session->set_flashdata('success', 'Data berhasil dihapus.');
        }else{
        	$this->session->set_flashdata('error', 'Data hanya bisa diupdate');
        }

        redirect('MsJabatan');
	}
}
