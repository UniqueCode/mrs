          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Fraksi</h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">

                    <ul class="nav navbar-right panel_toolbox">
                      <li class="invisible">
                        <a>&nbsp;</a>
                      </li>
                      <li class="invisible">
                        <a>&nbsp;</a>
                      </li>
                      <li class="invisible">
                        <a>&nbsp;</a>
                      </li>
                      <li>
                        <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form action="<?= $action; ?>" method="post" id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Periode</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" tabindex="-1" id="fk_periode_id" name="fk_periode_id" required>
                            <option value="">Pilih</option>
                            <?php foreach ($arrPeriode as $val) {?>
                            <option <?= $fk_periode_id==$val['id']?'selected':''?> value="<?=$val['id']?>"><?=$val['nama_periode']?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Nama Fraksi
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="nama_fraksi" name="nama_fraksi" required class="form-control col-md-7 col-xs-12" value="<?=$nama_fraksi?>">
                        </div>
                      </div>
                      <div class="form-group required">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">No Urut Fraksi
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="number" id="no_urut" name="no_urut" required class="form-control col-md-7 col-xs-12" value="<?=$no_urut?>">
                        </div>
                      </div>
                      <input type="hidden" name="id" value="<?=$id?>">
                      <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button type="reset" class="btn btn-primary">Batal</button>
                          <button type="submit" class="btn btn-success"><?= $button?></button>
                      </div>
                    </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>