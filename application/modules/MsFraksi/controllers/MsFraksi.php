<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MsFraksi extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('MMsPeriode');
		$this->load->model('MMsFraksi');
	}

	public function index(){
		$data=null;
		$data['fraksi'] = 'active';
		$data['act_add'] = base_url().'MsFraksi/create';
		$this->template->load('Home/template','MsFraksi/list',$data);
	}

	public function getDatatables(){
		header('Content-Type: application/json');
        
        $this->datatables->select('id,nama_fraksi,nama_periode,no_urut,status');
        $this->datatables->from("v_ms_fraksi");
        $this->db->order_by('no_urut','asc');
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('MsFraksi/update/$1'),'<i title="edit" class="glyphicon glyphicon-edit icon-white"></i>','class="btn btn-xs btn-success"').anchor(site_url('MsFraksi/delete/$1'),'<i title="hapus" class="glyphicon glyphicon-trash icon-white"></i>','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"').'</div>', 'id');

        echo $this->datatables->generate();
	}

	public function create(){
		$data = array(
			'action' => base_url().'MsFraksi/save',
			'button' => 'Simpan',
			'fk_periode_id' => set_value('fk_periode_id'),
			'nama_fraksi' => set_value('nama_fraksi'),
			'no_urut' => set_value('no_urut'),
			'id' => set_value('id'),
		);
		$data['fraksi'] = 'active';
		$data['arrPeriode'] = $this->MMsPeriode->get(array('status'=>1));
		$this->template->load('Home/template','MsFraksi/form',$data);
	}

	public function update($id){
		$kat = $this->MMsFraksi->get(array('id'=>$id));
		$kat = $kat[0];

		$data = array(
			'action' => base_url().'MsFraksi/save',
			'button' => 'Update',
			'fk_periode_id' => set_value('fk_periode_id',$kat['fk_periode_id']),
			'nama_fraksi' => set_value('nama_fraksi',$kat['nama_fraksi']),
			'no_urut' => set_value('no_urut',$kat['no_urut']),
			'id' => set_value('id',$kat['id']),
		);
		$data['fraksi'] = 'active';
		$data['arrPeriode'] = $this->MMsPeriode->get(array('status'=>1));
		$this->template->load('Home/template','MsFraksi/form',$data);
	}

	public function save(){
		$id = $this->input->post('id');
		$data['fk_periode_id'] = $this->input->post('fk_periode_id');
		$data['nama_fraksi'] = $this->input->post('nama_fraksi');
		$data['no_urut'] = $this->input->post('no_urut');

		if(empty($id)){
			$this->MMsFraksi->insert($data);
			$this->session->set_flashdata('success', 'Data Berhasil disimpan.');
		
		}else{
			$this->MMsFraksi->update($id,$data);
			$this->session->set_flashdata('success', 'Data Berhasil diupdate.');
		}
		
        redirect('MsFraksi');
	}

	public function delete($id){       
        $result = $this->MMsFraksi->delete($id);
		if($result){
        	$this->session->set_flashdata('success', 'Data berhasil dihapus.');
        }else{
        	$this->session->set_flashdata('error', 'Data hanya bisa diupdate');
        }

        redirect('MsFraksi');
	}
}
