<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MsAnggota extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('datatables');
		$this->load->model('MMsAnggota');
		$this->load->model('MUser');
		// $this->load->model('MMsAnggotaKeluarga');
		// $this->load->model('MMsAnggotaPendidikan');
		// $this->load->model('MMsAnggotaPekerjaan');
		// $this->load->model('MMsAnggotaJabatan');
		// $this->load->model('MMsAnggotaKesehatan');
	}

	public function index(){
		$data['anggota'] = 'active';
		$data['act_add'] = base_url().'MsAnggota/create';
		$this->template->load('Home/template','MsAnggota/list',$data);
	}

	public function getDatatables(){
		header('Content-Type: application/json');
        
        $this->datatables->select('id,nama,tempat_lahir,tgl_lhrnya,jk,no_tlp,alamat,nama_periode');
        $this->datatables->from("v_ms_anggota");
        $this->db->order_by('no_urut_anggota','asc');
        $this->datatables->add_column('action', '<div class="btn-group">'.anchor(site_url('MsAnggota/update/$1'),'<i title="edit" class="glyphicon glyphicon-edit icon-white"></i>','class="btn btn-xs btn-success"').anchor(site_url('MsAnggota/delete/$1'),'<i title="hapus" class="glyphicon glyphicon-trash icon-white"></i>','class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"').anchor(site_url('Home/index/$1'),'<i title="edit" class="glyphicon glyphicon-eye-open icon-white"></i>','class="btn btn-xs btn-primary"').'</div>', 'id');

        echo $this->datatables->generate();
	}

	public function create(){
		$data = array(
			'button' => 'Simpan',
			//diri
			'no_urut_anggota' => set_value('no_urut_anggota'),
			'nama' => set_value('nama'),
			'tempat_lahir' => set_value('tempat_lahir'),
			'tgl_lahir' => set_value('tgl_lahir'),
			'jenis_kel' => set_value('jenis_kel'),
			'agama' => set_value('agama'),
			'no_tlp' => set_value('no_tlp'),
			'alamat' => set_value('alamat'),
			'asal_parpol' => set_value('asal_parpol'),
			'asal_dapil' => set_value('asal_dapil'),
			'asal_fraksi' => set_value('asal_fraksi'),
			'hasil_suara' => set_value('hasil_suara'),
			'fk_periode_id' => set_value('fk_periode_id'),
			'foto_upload' => set_value('foto_upload'),
			'id_bpjs' => set_value('id_bpjs'),
			'id_username' => set_value('id_username'),
			'username' => set_value('username'),
			'id' => set_value('id'),
		);
		$data['arrAgama'] = $this->db->query("select * from ms_agama")->result();
		$data['arrParpol'] = $this->db->query("select * from ms_parpol")->result();
		$data['arrFraksi'] = $this->db->query("select * from v_ms_fraksi where status='Aktif'")->result();
		$data['arrPeriode'] = $this->db->query("select * from ms_periode where status=1")->result();
		$data['arrKeluarga'] = null;
		$data['arrPendidikan'] = null;
		$data['arrPekerjaan'] = null;
		$data['arrMsJabatan'] = $this->db->query("select * from ms_jabatan")->result();
		$data['arrMsPendidikan'] = $this->db->query("select * from ms_pendidikan")->result();
		$data['arrJabatan'] = null;
		$data['arrKesehatan'] = null;
		$data['anggota'] = 'active';
		$this->template->load('Home/template','MsAnggota/form',$data);
	}

	public function update($id){
		$kat = $this->MMsAnggota->get(array('id'=>$id));
		$kat = $kat[0];
		$usr = $this->MUser->get(array('fk_anggota_id'=>$id));

		$data = array(
			'button' => 'Update',
			'no_urut_anggota' => set_value('no_urut_anggota',$kat['no_urut_anggota']),
			'nama' => set_value('nama',$kat['nama']),
			'tempat_lahir' => set_value('tempat_lahir',$kat['tempat_lahir']),
			'tgl_lahir' => set_value('tgl_lahir',$this->help->ReverseTgl($kat['tgl_lahir'])),
			'jenis_kel' => set_value('jenis_kel',$kat['jenis_kel']),
			'agama' => set_value('agama',$kat['agama']),
			'no_tlp' => set_value('no_tlp',$kat['no_tlp']),
			'alamat' => set_value('alamat',$kat['alamat']),
			'asal_parpol' => set_value('asal_parpol',$kat['asal_parpol']),
			'asal_dapil' => set_value('asal_dapil',$kat['asal_dapil']),
			'asal_fraksi' => set_value('asal_fraksi',$kat['asal_fraksi']),
			'hasil_suara' => set_value('hasil_suara',$kat['hasil_suara']),
			'fk_periode_id' => set_value('fk_periode_id',$kat['fk_periode_id']),
			'foto_upload' => set_value('foto_upload',$kat['foto_upload']),
			'id_bpjs' => set_value('id_bpjs',$kat['id_bpjs']),
			'id_username' => set_value('id_username',$usr[0]['id']),
			'username' => set_value('username',$usr[0]['username']),
			'id' => set_value('id',$kat['id']),
		);

		$data['arrAgama'] = $this->db->query("select * from ms_agama")->result();
		$data['arrParpol'] = $this->db->query("select * from ms_parpol")->result();
		$data['arrFraksi'] = $this->db->query("select * from v_ms_fraksi where status='Aktif'")->result();
		$data['arrPeriode'] = $this->db->query("select * from ms_periode where status=1")->result();
		$data['arrHubungan'] = $this->db->query("select * from ms_hubungan")->result();
		// $data['arrKeluarga'] = $this->MMsAnggotaKeluarga->get(array('fk_anggota_id'=>$id));
		// $data['arrPendidikan'] = $this->MMsAnggotaPendidikan->get(array('fk_anggota_id'=>$id));
		// $data['arrPekerjaan'] = $this->MMsAnggotaPekerjaan->get(array('fk_anggota_id'=>$id));
		$data['arrMsJabatan'] = $this->db->query("select * from ms_jabatan")->result();
		$data['arrMsPendidikan'] = $this->db->query("select * from ms_pendidikan")->result();
		// $data['arrJabatan'] = $this->MMsAnggotaJabatan->get(array('fk_anggota_id'=>$id));
		// $data['arrKesehatan'] = $this->MMsAnggotaKesehatan->get(array('fk_anggota_id'=>$id));
		$this->template->load('Home/template','MsAnggota/form',$data);
	}

	public function save(){
		$id = $this->input->post('id');
		$diri = $this->input->post('data_diri');
		$keluarga = $this->input->post('data_keluarga');
		$rwyt_pddk = $this->input->post('riwayat_pendidikan');
		$rwyt_krj = $this->input->post('riwayat_pekerjaan');
		$rwyt_jbtn = $this->input->post('riwayat_jabatan');
		$rwyt_kshtn = $this->input->post('riwayat_kesehatan');

		$foto_diri = $_FILES['foto_upload']['name'];
		if($foto_diri){
			$ketFoto = substr($foto_diri,0,-4);
			$ketFoto = str_replace('.', '_', $ketFoto);
			$ketFoto = str_replace(' ', '_', $ketFoto);
			$extFoto = substr($foto_diri,-4);
			$ftDri = $ketFoto.$extFoto;
			$target_dir='image/anggota/'.$diri['fk_periode_id'];
			$this->help->mkdir_nya($target_dir);
			$namaFile = strtotime(date("Y-m-d H:i:s")).$ftDri;
			$config['upload_path'] = $target_dir;
			$config['file_name'] = $namaFile;
			$config['allowed_types'] = 'jpg|JPG';
			$config['max_size']	= '5000';
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('foto_upload')){
				die('Gagal upload file : '.$this->upload->display_errors());
				redirect('MsAnggota');
			}
			$foto_diri1 = $target_dir.'/'.$namaFile;
			$dataDiri['foto_upload'] = $foto_diri1;
		}

		$kartu_bpjs = $_FILES['id_bpjs']['name'];
		if($kartu_bpjs){
			$ketFoto = substr($kartu_bpjs,0,-4);
			$ketFoto = str_replace('.', '_', $ketFoto);
			$ketFoto = str_replace(' ', '_', $ketFoto);
			$extFoto = substr($kartu_bpjs,-4);
			$ftDri = $ketFoto.$extFoto;
			$target_dir='image/anggota/'.$diri['fk_periode_id'];
			$this->help->mkdir_nya($target_dir);
			$namaFile = strtotime(date("Y-m-d H:i:s")).$ftDri;
			$config['upload_path'] = $target_dir;
			$config['file_name'] = $namaFile;
			$config['allowed_types'] = 'jpg|JPG';
			$config['max_size']	= '5000';
			$this->load->library('upload', $config);
			if(!$this->upload->do_upload('id_bpjs')){
				die('Gagal upload file : '.$this->upload->display_errors());
				redirect('MsAnggota');
			}
			$foto_bpjs = $target_dir.'/'.$namaFile;
			$dataDiri['id_bpjs'] = $foto_bpjs;
		}

		$dataDiri['no_urut_anggota'] = $diri['no_urut_anggota'];
		$dataDiri['nama'] = $diri['nama'];
		$dataDiri['tempat_lahir'] = $diri['tempat_lahir'];
		$dataDiri['tgl_lahir'] = $this->help->ReverseTgl($diri['tgl_lahir']);
		$dataDiri['jenis_kel'] = $diri['jenis_kel'];
		$dataDiri['agama'] = $diri['agama'];
		$dataDiri['no_tlp'] = $diri['no_tlp'];
		$dataDiri['alamat'] = $diri['alamat'];
		$dataDiri['asal_parpol'] = $diri['asal_parpol'];
		$dataDiri['asal_dapil'] = $diri['asal_dapil'];
		$dataDiri['asal_fraksi'] = $diri['asal_fraksi'];
		$dataDiri['hasil_suara'] = $diri['hasil_suara'];
		$dataDiri['fk_periode_id'] = $diri['fk_periode_id'];
		$dataDiri['user_act'] = $this->session->id;
		$dataDiri['time_act'] = date('Y-m-d H:i:s');
		$dataDiri['email'] = $diri['username'];
		$usr['username'] = $diri['username'];
		$reset_password = isset($diri['reset_password'])?$diri['reset_password']:'';
		if($reset_password){
			$usr['password'] = md5('123456');
		}
		if($keluarga){
			$nama_keluarga = $keluarga['nama_keluarga'];
			$nik_keluarga = $keluarga['nik_keluarga'];
			$hubungan = $keluarga['hubungan'];
			$tempat_lahir_kel = $keluarga['tempat_lahir_kel'];
			$tgl_lahir_kel = $keluarga['tgl_lahir_kel'];
			$pendidikan = $keluarga['pendidikan'];
			$status_anak = $keluarga['status_anak'];
		}
		if($rwyt_pddk){
			$tingkat = $rwyt_pddk['tingkat'];
			$nama_pddk = $rwyt_pddk['nama_pddk'];
			$jurusan = $rwyt_pddk['jurusan'];
			$tahun_lulus = $rwyt_pddk['tahun_lulus'];
			$tempat_pddk = $rwyt_pddk['tempat_pddk'];
		}
		if($rwyt_krj){
			$nama_instansi = $rwyt_krj['nama_instansi'];
			$jenis_pekerjaan = $rwyt_krj['jenis_pekerjaan'];
			$lama_bekerja = $rwyt_krj['lama_bekerja'];
		}
		if($rwyt_jbtn){
			$fk_jabatan_id = $rwyt_jbtn['fk_jabatan_id'];
			$rw_nama_jabatan = $rwyt_jbtn['rw_nama_jabatan'];
			$tgl_mulai = $rwyt_jbtn['tgl_mulai'];
			$tgl_selesai = $rwyt_jbtn['tgl_selesai'];
			$keterangan_jabatan = $rwyt_jbtn['keterangan_jabatan'];
		}
		if($rwyt_kshtn){
			$jenis_sakit = $rwyt_kshtn['jenis_sakit'];
			$tgl_sakit = $rwyt_kshtn['tgl_sakit'];
			$tempat_perawatan = $rwyt_kshtn['tempat_perawatan'];
			$lama_perawatan = $rwyt_kshtn['lama_perawatan'];
			$dokter_yg_menangani = $rwyt_kshtn['dokter_yg_menangani'];
			$keterangan = $rwyt_kshtn['keterangan'];
		}

		if(empty($id)){
			$this->db->trans_start();
				$this->MMsAnggota->insert($dataDiri);
				$idAnggota = $this->db->insert_id();	

				$usr['password'] = md5('123456');
				$usr['nama_lengkap'] =$diri['nama'];
				$usr['level'] =2;
				$usr['fk_anggota_id'] = $idAnggota;
				$usr['blokir'] ='N';
				$usr['waktu_buat'] = date('Y-m-d H:i:s');
				$usr['user_act'] = $this->session->id;
				$usr['time_act'] = date('Y-m-d H:i:s');
				$this->MUser->insert($usr);

				if($keluarga){
					foreach ($nama_keluarga as $key => $val) {
						$dataDetailKel[] = array(
							'fk_anggota_id'=>$idAnggota,
							'nama_keluarga'=>$val,
							'nik_keluarga'=>$nik_keluarga[$key],
							'hubungan'=>$hubungan[$key],
							'tempat_lahir_kel'=>$tempat_lahir_kel[$key],
							'tgl_lahir_kel'=>$this->help->ReverseTgl($tgl_lahir_kel[$key]),
							'pendidikan'=>$pendidikan[$key],
							'status_anak'=>$status_anak[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_keluarga', $dataDetailKel);
				}
				if($rwyt_pddk){
					foreach ($tingkat as $key => $val2) {
						$dataDetailPddk[] = array(
							'fk_anggota_id'=>$idAnggota,
							'tingkat'=>$val2,
							'nama_pddk'=>$nama_pddk[$key],
							'jurusan'=>$jurusan[$key],
							'tahun_lulus'=>$tahun_lulus[$key],
							'tempat_pddk'=>$tempat_pddk[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_pendidikan', $dataDetailPddk);
				}
				if($rwyt_krj){
					foreach ($nama_instansi as $key => $val3) {
						$dataDetailKrj[] = array(
							'fk_anggota_id'=>$idAnggota,
							'nama_instansi'=>$val3,
							'jenis_pekerjaan'=>$jenis_pekerjaan[$key],
							'lama_bekerja'=>$lama_bekerja[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_pekerjaan', $dataDetailKrj);
				}
				if($rwyt_jbtn){
					foreach ($fk_jabatan_id as $key => $val4) {
						$dataDetailJbt[] = array(
							'fk_anggota_id'=>$idAnggota,
							'fk_jabatan_id'=>$val4,
							'rw_nama_jabatan'=>$rw_nama_jabatan[$key],
							'tgl_mulai'=>$tgl_mulai[$key],
							'tgl_selesai'=>$tgl_selesai[$key],
							'keterangan_jabatan'=>$keterangan_jabatan[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_jabatan', $dataDetailJbt);
				}
				if($rwyt_kshtn){
					foreach ($jenis_sakit as $key => $val5) {
						$dataDetailKes[] = array(
							'fk_anggota_id'=>$idAnggota,
							'jenis_sakit'=>$val5,
							'tgl_sakit'=>$this->help->ReverseTgl($tgl_sakit[$key]),
							'tempat_perawatan'=>$tempat_perawatan[$key],
							'lama_perawatan'=>$lama_perawatan[$key],
							'dokter_yg_menangani'=>$dokter_yg_menangani[$key],
							'keterangan'=>$keterangan[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_kesehatan', $dataDetailKes);
				}

			$this->db->trans_complete();			
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();
			    $this->session->set_flashdata('error', 'Data Gagal disimpan.');
			} 
			else {
			    $this->db->trans_commit();
				$this->session->set_flashdata('success', 'Data Berhasil disimpan.');
			}
		
		}else{ //update
			$this->db->trans_start();
				$this->MUser->update($diri['id_username'],$usr);

				$this->MMsAnggota->update($id,$dataDiri);

				if($keluarga){
					foreach ($nama_keluarga as $key => $val) {
						$dataDetailKel[] = array(
							'fk_anggota_id'=>$id,
							'nama_keluarga'=>$val,
							'nik_keluarga'=>$nik_keluarga[$key],
							'hubungan'=>$hubungan[$key],
							'tempat_lahir_kel'=>$tempat_lahir_kel[$key],
							'tgl_lahir_kel'=>$this->help->ReverseTgl($tgl_lahir_kel[$key]),
							'pendidikan'=>$pendidikan[$key],
							'status_anak'=>$status_anak[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_keluarga', $dataDetailKel);
				}
				if($rwyt_pddk){
					foreach ($tingkat as $key => $val2) {
						$dataDetailPddk[] = array(
							'fk_anggota_id'=>$id,
							'tingkat'=>$val2,
							'nama_pddk'=>$nama_pddk[$key],
							'jurusan'=>$jurusan[$key],
							'tahun_lulus'=>$tahun_lulus[$key],
							'tempat_pddk'=>$tempat_pddk[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_pendidikan', $dataDetailPddk);
				}
				if($rwyt_krj){
					foreach ($nama_instansi as $key => $val3) {
						$dataDetailKrj[] = array(
							'fk_anggota_id'=>$id,
							'nama_instansi'=>$val3,
							'jenis_pekerjaan'=>$jenis_pekerjaan[$key],
							'lama_bekerja'=>$lama_bekerja[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_pekerjaan', $dataDetailKrj);
				}
				if($rwyt_jbtn){
					foreach ($fk_jabatan_id as $key => $val4) {
						$dataDetailJbt[] = array(
							'fk_anggota_id'=>$id,
							'fk_jabatan_id'=>$val4,
							'rw_nama_jabatan'=>$rw_nama_jabatan[$key],
							'tgl_mulai'=>$tgl_mulai[$key],
							'tgl_selesai'=>$tgl_selesai[$key],
							'keterangan_jabatan'=>$keterangan_jabatan[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_jabatan', $dataDetailJbt);
				}
				if($rwyt_kshtn){
					foreach ($jenis_sakit as $key => $val5) {
						$dataDetailKes[] = array(
							'fk_anggota_id'=>$id,
							'jenis_sakit'=>$val5,
							'tgl_sakit'=>$this->help->ReverseTgl($tgl_sakit[$key]),
							'tempat_perawatan'=>$tempat_perawatan[$key],
							'lama_perawatan'=>$lama_perawatan[$key],
							'dokter_yg_menangani'=>$dokter_yg_menangani[$key],
							'keterangan'=>$keterangan[$key],
							'user_act'=>$this->session->id,
							'time_act'=>date('Y-m-d H:i:s'),
						);
					}
					$this->db->insert_batch('ms_anggota_kesehatan', $dataDetailKes);
				}

			$this->db->trans_complete();			
			if ($this->db->trans_status() === FALSE) {
			    $this->db->trans_rollback();
			    $this->session->set_flashdata('error', 'Data Gagal disimpan.');
			} 
			else {
			    $this->db->trans_commit();
				$this->session->set_flashdata('success', 'Data Berhasil diupdate.');
			}
		}
		
        redirect('MsAnggota');
	}

	public function delete($id){       
        $result = $this->MMsAnggota->delete($id);
		if($result){
        	$this->session->set_flashdata('success', 'Data berhasil dihapus.');
        }else{
        	$this->session->set_flashdata('error', 'Data hanya bisa diupdate');
        }

        redirect('MsAnggota');
	}

	// public function delDetailHub($fk_anggota_id, $id){
	// 	$result = $this->MMsAnggotaKeluarga->delete($id);
	// 	if($result){
 //        	$this->session->set_flashdata('success', 'Data Keluarga berhasil dihapus.');
 //        }else{
 //        	$this->session->set_flashdata('error', 'Data Keluarga gagal dihapus.');
 //        }
 //        redirect('MsAnggota/update/'.$fk_anggota_id);
	// }
	// public function delDetailRwPddk($fk_anggota_id, $id){
	// 	$result = $this->MMsAnggotaPendidikan->delete($id);
	// 	if($result){
 //        	$this->session->set_flashdata('success', 'Data Riwayat Pendidikan berhasil dihapus.');
 //        }else{
 //        	$this->session->set_flashdata('error', 'Data Riwayat Pendidikan gagal dihapus.');
 //        }
 //        redirect('MsAnggota/update/'.$fk_anggota_id);
	// }
	// public function delDetailRwKrj($fk_anggota_id, $id){
	// 	$result = $this->MMsAnggotaPekerjaan->delete($id);
	// 	if($result){
 //        	$this->session->set_flashdata('success', 'Data Riwayat Pekerjaan berhasil dihapus.');
 //        }else{
 //        	$this->session->set_flashdata('error', 'Data Riwayat Pekerjaan gagal dihapus.');
 //        }
 //        redirect('MsAnggota/update/'.$fk_anggota_id);
	// }
	// public function delDetailRwJbtn($fk_anggota_id, $id){
	// 	$result = $this->MMsAnggotaJabatan->delete($id);
	// 	if($result){
 //        	$this->session->set_flashdata('success', 'Data Riwayat Jabatan berhasil dihapus.');
 //        }else{
 //        	$this->session->set_flashdata('error', 'Data Riwayat Jabatan gagal dihapus.');
 //        }
 //        redirect('MsAnggota/update/'.$fk_anggota_id);
	// }
	// public function delDetailRwKshtn($fk_anggota_id, $id){
	// 	$result = $this->MMsAnggotaKesehatan->delete($id);
	// 	if($result){
 //        	$this->session->set_flashdata('success', 'Data Riwayat Jabatan berhasil dihapus.');
 //        }else{
 //        	$this->session->set_flashdata('error', 'Data Riwayat Jabatan gagal dihapus.');
 //        }
 //        redirect('MsAnggota/update/'.$fk_anggota_id);
	// }

	// public function view($id){
	// 	$data['id']=$id;
		
	// 	$this->template->load('Home/template','MsAnggota/view_data_diri',$data);
	// }

	// public function viewDataDiri(){
	// 	$id = $this->input->post('id');
	// 	$anggota = $this->db->query("select * from v_ms_anggota WHERE id=".$id."")->row();

	// 	$dataAng['anggota'] = $anggota;
	// 	$dataAng['arrKeluarga'] = $this->MMsAnggotaKeluarga->get(array('fk_anggota_id'=>$id));
	// 	$dataAng['arrPendidikan'] = $this->MMsAnggotaPendidikan->get(array('fk_anggota_id'=>$id));
	// 	$dataAng['arrPekerjaan'] = $this->MMsAnggotaPekerjaan->get(array('fk_anggota_id'=>$id));
	// 	$dataAng['arrJabatan'] = $this->MMsAnggotaJabatan->get(array('fk_anggota_id'=>$id));
	// 	$dataAng['arrKesehatan'] = $this->MMsAnggotaKesehatan->get(array('fk_anggota_id'=>$id));
	// 	$this->load->view('Home/biodata_anggota',$dataAng);
	// }
	
}
