<section class="content-header">
    <div class="row">
        <div class="col-md-12">
            <div class="judul">View Data Anggota</div>
        </div>
    </div>
    <br>
    <div>
        <a href="<?=base_url()?>MsAnggota" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-chevron-left"></i> kembali</a>
    </div>
</section>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-user"></i> Data Pribadi</h2>
                <div class="box-icon">
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div id="tampilData"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){    
	id="<?=$id?>";
    $.ajax({
        type:'post',
        url: "<?php echo base_url()?>MsAnggota/viewDataDiri",
        data:{id},
        dataType: "html",
        success: function(data){
            $("#tampilData").html(data);
        }
    });
});
</script>