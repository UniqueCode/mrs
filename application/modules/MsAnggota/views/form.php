<section class="content-header">
    <div class="row">
        <div class="col-md-12">
            <div class="judul">Anggota</div>
        </div>
    </div>
</section>
<section class="content">
    <div class="box">
    <div class="box-body">
    <div class="row">
        <div class="col-md-12"><br>
        <?php if ($this->session->flashdata('success')): ?>
            <div class="alert alert-success">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Sukses!</strong> <?php echo $this->session->flashdata('success') ?>
            </div>
        <?php endif; ?>
        <?php if ($this->session->flashdata('error')): ?>
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Error!</strong> <?php echo $this->session->flashdata('error') ?>
            </div>
        <?php endif; ?>
        <div class="box-inner">
            <form action="<?=base_url()?>MsAnggota/save" method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#data_diri" data-toggle="tab">Data Diri</a></li>
                    <li><a href="#riwayat_pendidikan" data-toggle="tab">Riwayat Pendidikan</a></li>
                    <li><a href="#riwayat_pekerjaan" data-toggle="tab">Riwayat Pekerjaan</a></li>
                    <li><a href="#riwayat_jabatan" data-toggle="tab">Riwayat Jabatan</a></li>
                    <li><a href="#riwayat_kesehatan" data-toggle="tab">Riwayat Kesehatan</a></li>
                    <li><a href="#data_keluarga" data-toggle="tab">Data Keluarga</a></li>
                </ul>
                <div class="tab-content">
                    <br>                    
                    <div class="tab-pane active" id="data_diri">
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label class="col-md-3 control-label">No Urut</label>
                                <div class="col-md-7">
                                     <input name="data_diri[no_urut_anggota]" class="form-control" value="<?=$no_urut_anggota?>" required></input>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Nama</label>
                                <div class="col-md-7">
                                     <input name="data_diri[nama]" class="form-control" value="<?=$nama?>" required></input>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Tempat Lahir</label>
                                <div class="col-md-7">
                                     <input name="data_diri[tempat_lahir]" class="form-control" value="<?=$tempat_lahir?>" required></input>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Tgl Lahir</label>
                                <div class="col-md-7">
                                     <input name="data_diri[tgl_lahir]" class="form-control tanggal" value="<?=$tgl_lahir?>" required></input>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Jenis Kelamin</label>
                                <div class="col-md-7">
                                    <select class="form-control chosen" name="data_diri[jenis_kel]" required>
                                        <option value="">Pilih</option>
                                        <option <?= $jenis_kel=='L'?'selected':'';?> value="L">Laki-laki</option>
                                        <option <?= $jenis_kel=='P'?'selected':'';?> value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Agama</label>
                                <div class="col-md-7">
                                    <select class="form-control chosen" name="data_diri[agama]" required>
                                        <option value="">Pilih</option>
                                        <?php foreach ($arrAgama as $val) {?>
                                            <option <?= $agama==$val->nama?'selected':''?> value="<?=$val->nama?>"><?=$val->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">No Tlp</label>
                                <div class="col-md-7">
                                     <input name="data_diri[no_tlp]" class="form-control" value="<?=$no_tlp?>" required></input>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Alamat</label>
                                <div class="col-md-7">
                                     <textarea name="data_diri[alamat]" class="form-control" required><?=$alamat?></textarea>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Asal Parpol</label>
                                <div class="col-md-7">
                                    <select class="form-control chosen" name="data_diri[asal_parpol]" required>
                                        <option value="">Pilih</option>
                                        <?php foreach ($arrParpol as $val2) {?>
                                            <option <?= $asal_parpol==$val2->nama_parpol?'selected':''?> value="<?=$val2->nama_parpol?>"><?=$val2->nama_parpol?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Asal Dapil</label>
                                <div class="col-md-7">
                                     <input name="data_diri[asal_dapil]" class="form-control" value="<?=$asal_dapil?>" required></input>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Asal Fraksi</label>
                                <div class="col-md-7">
                                    <select class="form-control chosen" name="data_diri[asal_fraksi]" required>
                                        <option value="">Pilih</option>
                                        <?php foreach ($arrFraksi as $val3) {?>
                                            <option <?= $asal_fraksi==$val3->nama_fraksi?'selected':''?> value="<?=$val3->nama_fraksi?>"><?=$val3->nama_fraksi?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Perolehan Suara</label>
                                <div class="col-md-7">
                                     <input name="data_diri[hasil_suara]" class="form-control" value="<?=$hasil_suara?>" required></input>
                                </div>
                            </div>
                            <div class="form-group required">
                                <label class="col-md-3 control-label">Periode</label>
                                <div class="col-md-7">
                                    <select class="form-control chosen" name="data_diri[fk_periode_id]" required>
                                        <option value="">Pilih</option>
                                        <?php foreach ($arrPeriode as $val4) {?>
                                            <option <?= $fk_periode_id==$val4->id?'selected':''?> value="<?=$val4->id?>"><?=$val4->nama_periode?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-3 control-label">Foto</label>
                                <div class="col-sm-6">
                                    <?php if(!empty($foto_upload)){ $ft = explode('/', $foto_upload); ?>
                                        <input type="text" class="form-control" value="<?= $ft[3];?>" readonly><i style="color: red">*</i> current file<br/>
                                    <?php } ?>
                                    <input type="file" name="foto_upload" accept=".jpg">
                                    <p class="text-danger" style="font-size: 10px; margin: 5px 0px;">Size Max 5 Mb, Type JPG</p>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="col-sm-3 control-label">Scan ID BPJS</label>
                                <div class="col-sm-6">
                                    <?php if(!empty($id_bpjs)){ $ftb = explode('/', $id_bpjs); ?>
                                        <input type="text" class="form-control" value="<?= $ftb[3];?>" readonly><i style="color: red">*</i> current file<br/>
                                    <?php } ?>
                                    <input type="file" name="id_bpjs" accept=".jpg">
                                    <p class="text-danger" style="font-size: 10px; margin: 5px 0px;">Size Max 5 Mb, Type JPG</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="required"><label class="col-md-3 control-label">Username (email aktif)</label></div>
                                <!-- <div class="input-group"> -->
                                    <div class="col-md-5">
                                         <input type="hidden" name="data_diri[id_username]" class="form-control" value="<?=$id_username?>" required></input>
                                         <input type="email" name="data_diri[username]" class="form-control" value="<?=$username?>" required></input>
                                    </div>
                                    <div class="col-md-5 control-label">
                                        <?php if(!$id){?>
                                            <span class="label-success label label-default">Password awal : 123456</span>
                                        <?php }?>
                                    </div>
                                <!-- </div> -->
                            </div>
                            <?php if($id):?>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Reset Password</label>
                                    <div class="col-md-7">
                                        <div class="input-group">
                                            <div class="col-md-1">
                                                <input name="data_diri[reset_password]" id="reset_password" type="checkbox">
                                            </div>
                                            <div class="col-md-10 control-label" style="display: none" id="info_pswd">
                                                <span class="label-success label label-default">Password awal : 123456</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="tab-pane" id="riwayat_pendidikan">
                        <?php if(isset($arrPendidikan)): ?>
                            <div class="col-md-1"></div>
                            <div class="panel panel-default col-md-10">
                                <div class="table-responsive">                     
                                    <table class="table table-bordered table-striped example_data">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center" width="40px">No</th>
                                                <th style="text-align: center">Tingkat</th>
                                                <th style="text-align: center">Nama Pendidikan</th>
                                                <th style="text-align: center">Jurusan</th>
                                                <th style="text-align: center">Tahun Lulus</th>
                                                <th style="text-align: center">Tempat Pendidikan</th>
                                                <th style="text-align: center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1;?>
                                            <?php foreach($arrPendidikan as $valPdk) :?>
                                                <tr>
                                                    <td class="text-center"><?php echo $no++;?></td>
                                                    <td class="text-center"><?=$valPdk['tingkat']?></td>
                                                    <td class="text-center"><?=$valPdk['nama_pddk']?></td>
                                                    <td class="text-center"><?=$valPdk['jurusan']?></td>
                                                    <td class="text-center"><?=$valPdk['tahun_lulus']?></td>
                                                    <td class="text-center"><?=$valPdk['tempat_pddk']?></td>
                                                    <td class="text-center"><a href="<?=base_url()?>MsAnggota/delDetailRwPddk/<?=$valPdk['fk_anggota_id']?>/<?=$valPdk['id']?>" class="btn btn-xs btn-danger" title="Hapus" onclick="javasciprt: return confirm('Apakah anda yakin?')" ><i class="glyphicon glyphicon-trash icon-white"></i></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tingkat</label>
                                <div class="col-md-8">
                                    <select class="form-control chosen kosong2" id="rw_tingkat">
                                        <option value="">Pilih</option>
                                        <?php foreach ($arrMsPendidikan as $valPdk) {?>
                                            <option value="<?=$valPdk->nama?>"><?=$valPdk->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Pendidikan</label>
                                <div class="col-md-8">
                                     <input id="rw_nama_pddk" class="form-control kosong2" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Jurusan</label>
                                <div class="col-md-8">
                                     <input id="rw_jurusan" class="form-control kosong2" ></input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tahun Lulus</label>
                                <div class="col-md-8">
                                     <input id="rw_tahun_lulus" class="form-control kosong2 tahun" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tempat Pendidikan</label>
                                <div class="col-md-8">
                                     <input id="rw_tempat_pddk" class="form-control kosong2" ></input>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-6" align="center">
                                <a id="reset_2" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</a>
                                <a id="tambah_2" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i> Tambahkan Ke List</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default">
                                <table class="table table-bordered table-striped" >
                                    <tr style="background-color: #d5d2d1">
                                        <th class="text-center">Tingkat</th>
                                        <th class="text-center">Nama Pendidikan</th>
                                        <th class="text-center">Jurusan</th>
                                        <th class="text-center">Tahun Lulus</th>
                                        <th class="text-center">Tempat Pendidikan</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                    <tbody id="tampilDetail_2"></tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="riwayat_pekerjaan">
                        <?php if(isset($arrPekerjaan)): ?>
                            <div class="col-md-1"></div>
                            <div class="panel panel-default col-md-10">
                                <div class="table-responsive">                     
                                    <table class="table table-bordered table-striped example_data">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center" width="40px">No</th>
                                                <th style="text-align: center">Jenis Pekerjaan</th>
                                                <th style="text-align: center">Nama Instansi</th>
                                                <th style="text-align: center">Lama Bekerja</th>
                                                <th style="text-align: center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1;?>
                                            <?php foreach($arrPekerjaan as $valPkr) :?>
                                                <tr>
                                                    <td class="text-center"><?php echo $no++;?></td>
                                                    <td class="text-center"><?=$valPkr['jenis_pekerjaan']?></td>
                                                    <td class="text-center"><?=$valPkr['nama_instansi']?></td>
                                                    <td class="text-center"><?=$valPkr['lama_bekerja']?></td>
                                                    <td class="text-center"><a href="<?=base_url()?>MsAnggota/delDetailRwKrj/<?=$valPkr['fk_anggota_id']?>/<?=$valPkr['id']?>" class="btn btn-xs btn-danger" title="Hapus" onclick="javasciprt: return confirm('Apakah anda yakin?')" ><i class="glyphicon glyphicon-trash icon-white"></i></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Jenis Pekerjaan</label>
                                <div class="col-md-8">
                                     <input id="krj_jenis" class="form-control kosong3" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Instansi</label>
                                <div class="col-md-8">
                                     <input id="krj_instansi" class="form-control kosong3" ></input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Lama Bekerja</label>
                                <div class="col-md-8">
                                     <input id="krj_lama_kerja" class="form-control kosong3" ></input>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-6" align="center">
                                <a id="reset_3" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</a>
                                <a id="tambah_3" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i> Tambahkan Ke List</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default">
                                <table class="table table-bordered table-striped" >
                                    <tr style="background-color: #d5d2d1">
                                        <th class="text-center">Jenis Pekerjaan</th>
                                        <th class="text-center">Nama Instansi</th>
                                        <th class="text-center">Lama Bekerja</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                    <tbody id="tampilDetail_3"></tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="riwayat_jabatan">
                        <?php if(isset($arrJabatan)): ?>
                            <div class="col-md-1"></div>
                            <div class="panel panel-default col-md-10">
                                <div class="table-responsive">                     
                                    <table class="table table-bordered table-striped example_data">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center" width="40px">No</th>
                                                <th style="text-align: center">Jabatan</th>
                                                <th style="text-align: center">Tgl Mulai</th>
                                                <th style="text-align: center">Tgl Selesai</th>
                                                <th style="text-align: center">Keterangan</th>
                                                <th style="text-align: center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1;?>
                                            <?php foreach($arrJabatan as $valJbt) :?>
                                                <tr>
                                                    <td class="text-center"><?php echo $no++;?></td>
                                                    <td class="text-center"><?=$valJbt['rw_nama_jabatan']?></td>
                                                    <td class="text-center"><?=$valJbt['tgl_mulai']?></td>
                                                    <td class="text-center"><?=$valJbt['tgl_selesai']?></td>
                                                    <td class="text-center"><?=$valJbt['keterangan_jabatan']?></td>
                                                    <td class="text-center"><a href="<?=base_url()?>MsAnggota/delDetailRwJbtn/<?=$valJbt['fk_anggota_id']?>/<?=$valJbt['id']?>" class="btn btn-xs btn-danger" title="Hapus" onclick="javasciprt: return confirm('Apakah anda yakin?')" ><i class="glyphicon glyphicon-trash icon-white"></i></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Jabatan</label>
                                <div class="col-md-8">
                                    <select class="form-control chosen kosong4" id="jbt_nama_jabatan">
                                        <option value="">Pilih</option>
                                        <?php foreach ($arrMsJabatan as $valJab) {?>
                                            <option value="<?=empty($valJab->nama_sub_jabatan)?$valJab->id.'|'.$valJab->nama_jabatan:$valJab->id.'|'.$valJab->nama_jabatan.' - '.$valJab->nama_sub_jabatan?>"><?=empty($valJab->nama_sub_jabatan)?$valJab->nama_jabatan:$valJab->nama_jabatan.' - '.$valJab->nama_sub_jabatan?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tgl Mulai</label>
                                <div class="col-md-8">
                                    <input id="jbt_tgl_mulai" class="form-control kosong4 month_name" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tgl Selesai</label>
                                <div class="col-md-8">
                                    <select class="form-control kosong4" id="pilih_tgl_selesai">
                                        <option value="Sekarang">Sekarang</option>
                                        <option value="tgl">Tanggal</option>
                                    </select>
                                    <br>
                                    <div id="tgl_slsi" style="display: none"><input id="jbt_tgl_selesai" class="form-control kosong4 month_name" ></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control kosong4" id="jbt_keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-6" align="center">
                                <a id="reset_4" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</a>
                                <a id="tambah_4" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i> Tambahkan Ke List</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default">
                                <table class="table table-bordered table-striped" >
                                    <tr style="background-color: #d5d2d1">
                                        <th class="text-center">Jabatan</th>
                                        <th class="text-center">Tgl Mulai</th>
                                        <th class="text-center">Tgl Selesai</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                    <tbody id="tampilDetail_4"></tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="riwayat_kesehatan">
                        <?php if(isset($arrKesehatan)): ?>
                            <div class="col-md-1"></div>
                            <div class="panel panel-default col-md-10">
                                <div class="table-responsive">                     
                                    <table class="table table-bordered table-striped example_data">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center" width="40px">No</th>
                                                <th style="text-align: center">Jenis Sakit</th>
                                                <th style="text-align: center">Tgl Sakit</th>
                                                <th style="text-align: center">Tempat Perawatan</th>
                                                <th style="text-align: center">Lama Perawatan</th>
                                                <th style="text-align: center">Dokter Yg Menangani</th>
                                                <th style="text-align: center">Keterangan</th>
                                                <th style="text-align: center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1;?>
                                            <?php foreach($arrKesehatan as $valKes) :?>
                                                <tr>
                                                    <td class="text-center"><?php echo $no++;?></td>
                                                    <td class="text-center"><?=$valKes['jenis_sakit']?></td>
                                                    <td class="text-center"><?=$this->help->ReverseTgl($valKes['tgl_sakit'])?></td>
                                                    <td class="text-center"><?=$valKes['tempat_perawatan']?></td>
                                                    <td class="text-center"><?=$valKes['lama_perawatan']?></td>
                                                    <td class="text-center"><?=$valKes['dokter_yg_menangani']?></td>
                                                    <td class="text-center"><?=$valKes['keterangan']?></td>
                                                    <td class="text-center"><a href="<?=base_url()?>MsAnggota/delDetailRwKshtn/<?=$valKes['fk_anggota_id']?>/<?=$valKes['id']?>" class="btn btn-xs btn-danger" title="Hapus" onclick="javasciprt: return confirm('Apakah anda yakin?')" ><i class="glyphicon glyphicon-trash icon-white"></i></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Jenis Sakit</label>
                                <div class="col-md-8">
                                    <input id="kes_jenis_sakit" class="form-control kosong5" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tgl Sakit</label>
                                <div class="col-md-8">
                                    <input id="kes_tgl_sakit" class="form-control kosong5 tanggal" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tempat Perawatan</label>
                                <div class="col-md-8">
                                    <input id="kes_tempat_perawatan" class="form-control kosong5" ></input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Lama Perawatan</label>
                                <div class="col-md-8">
                                    <input id="kes_lama_perawatan" class="form-control kosong5" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Dokter Yg Menangani</label>
                                <div class="col-md-8">
                                    <input id="kes_dokter_yg_menangani" class="form-control kosong5" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control kosong5" id="kes_keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-6" align="center">
                                <a id="reset_5" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</a>
                                <a id="tambah_5" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i> Tambahkan Ke List</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default">
                                <table class="table table-bordered table-striped" >
                                    <tr style="background-color: #d5d2d1">
                                        <th class="text-center">Jenis Sakit</th>
                                        <th class="text-center">Tgl Sakit</th>
                                        <th class="text-center">Tempat Perawatan</th>
                                        <th class="text-center">Lama Perawatan</th>
                                        <th class="text-center">Dokter Yg Menangani</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                    <tbody id="tampilDetail_5"></tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="data_keluarga">
                        <?php if(isset($arrKeluarga)): ?>
                            <div class="col-md-1"></div>
                            <div class="panel panel-default col-md-10">
                                <div class="table-responsive">                     
                                    <table class="table table-bordered table-striped example_data">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center" width="40px">No</th>
                                                <th style="text-align: center">Nama Suami/Istri/Anak</th>
                                                <th style="text-align: center">NIK</th>
                                                <th style="text-align: center">Hubungan</th>
                                                <th style="text-align: center">Tempat Lahir</th>
                                                <th style="text-align: center">Tgl Lahir</th>
                                                <!-- <th style="text-align: center">Pendidikan</th> -->
                                                <th style="text-align: center">Status Anak</th>
                                                <th style="text-align: center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1;?>
                                            <?php foreach($arrKeluarga as $valHub) :?>
                                                <tr>
                                                    <td class="text-center"><?php echo $no++;?></td>
                                                    <td class="text-center"><?=$valHub['nama_keluarga']?></td>
                                                    <td class="text-center"><?=$valHub['nik_keluarga']?></td>
                                                    <td class="text-center"><?=$valHub['hubungan']?></td>
                                                    <td class="text-center"><?=$valHub['tempat_lahir_kel']?></td>
                                                    <td class="text-center"><?=$this->help->ReverseTgl($valHub['tgl_lahir_kel'])?></td>
                                                    <!-- <td class="text-center"><?=$valHub['pendidikan']?></td> -->
                                                    <td class="text-center"><?=$valHub['status_anak']?></td>
                                                    <td class="text-center"><a href="<?=base_url()?>MsAnggota/delDetailHub/<?=$valHub['fk_anggota_id']?>/<?=$valHub['id']?>" class="btn btn-xs btn-danger" title="Hapus" onclick="javasciprt: return confirm('Apakah anda yakin?')" ><i class="glyphicon glyphicon-trash icon-white"></i></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nama Suami/Istri/Anak</label>
                                <div class="col-md-8">
                                     <input id="nama_kel" class="form-control kosong1" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">NIK</label>
                                <div class="col-md-8">
                                     <input id="nik_kel" class="form-control kosong1" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Hubungan</label>
                                <div class="col-md-8">
                                    <select class="form-control chosen kosong1" id="hub_kel">
                                        <option value="">Pilih</option>
                                        <?php foreach ($arrHubungan as $val5) {?>
                                            <option value="<?=$val5->nama_hubungan?>"><?=$val5->nama_hubungan?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tempat Lahir</label>
                                <div class="col-md-8">
                                     <input id="tempat_lahir_kel" class="form-control kosong1" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tgl Lahir</label>
                                <div class="col-md-8">
                                     <input id="tgl_lahir_kel" class="form-control tanggal kosong1" ></input>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label class="col-md-4 control-label">Pendidikan</label>
                                <div class="col-md-8">
                                    <select class="form-control chosen kosong1" id="pddkn">
                                        <option value="">Pilih</option>
                                        <?php foreach ($arrMsPendidikan as $valPdk) {?>
                                            <option value="<?=$valPdk->nama?>"><?=$valPdk->nama?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div> -->
                            <div class="form-group">
                                <label class="col-md-4 control-label">Status Anak</label>
                                <div class="col-md-8">
                                    <select class="form-control chosen kosong1" id="status_anak">
                                        <option value="">Pilih</option>
                                        <option value="Belum Nikah">Belum Nikah</option>
                                        <option value="Sudah Nikah">Sudah Nikah</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-6" align="center">
                                <a id="reset_1" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</a>
                                <a id="tambah_1" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i> Tambahkan Ke List</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default">
                                <table class="table table-bordered table-striped" >
                                    <tr style="background-color: #d5d2d1">
                                        <th class="text-center">Nama Suami/Istri/Anak</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Hubungan</th>
                                        <th class="text-center">Tempat Lahir</th>
                                        <th class="text-center">Tgl Lahir</th>
                                        <th class="text-center">Pendidikan</th>
                                        <th class="text-center">Status Anak</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                    <tbody id="tampilDetail_1"></tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="dokumentasi">
                        <?php if(isset($arrDokumentasi)): ?>
                            <div class="col-md-1"></div>
                            <div class="panel panel-default col-md-10">
                                <div class="table-responsive">                     
                                    <table class="table table-bordered table-striped example_data">
                                        <thead>
                                            <tr>
                                                <th style="text-align: center" width="40px">No</th>
                                                <th style="text-align: center">Judul</th>
                                                <th style="text-align: center">Tanggal</th>
                                                <th style="text-align: center">File</th>
                                                <th style="text-align: center">Keterangan</th>
                                                <th style="text-align: center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no=1;?>
                                            <?php foreach($arrDokumentasi as $valDok) :?>
                                                <tr>
                                                    <td class="text-center"><?php echo $no++;?></td>
                                                    <td class="text-center"><?=$valDok['judul']?></td>
                                                    <td class="text-center"><?=$this->help->ReverseTgl($valDok['tgl_dokumentasi'])?></td>
                                                    <td class="text-center">
                                                        <?php if($valDok['file_dokumentasi']){?>
                                                            <a href="<?=base_url().$valDok['file_dokumentasi']?>" target="_blank"><i class="glyphicon glyphicon-file icon-white"></i></a>
                                                        <?php } ?>
                                                    </td>
                                                    <td class="text-center"><?=$valDok['keterangan_dokumentasi']?></td>
                                                    <td class="text-center"><a href="<?=base_url()?>MsAnggota/delDetailDok/<?=$valDok['fk_anggota_id']?>/<?=$valDok['id']?>" class="btn btn-xs btn-danger" title="Hapus" onclick="javasciprt: return confirm('Apakah anda yakin?')" ><i class="glyphicon glyphicon-trash icon-white"></i></a></td>
                                                </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Judul</label>
                                <div class="col-md-8">
                                     <input id="dok_judul" class="form-control kosong7" ></input>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Tanggal</label>
                                <div class="col-md-8">
                                     <input id="dok_tgl" class="form-control kosong7 tanggal" ></input>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Keterangan</label>
                                <div class="col-md-8">
                                    <textarea class="form-control kosong7" id="dok_keterangan"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-2"></div>
                            <div class="col-md-6" align="center">
                                <a id="reset_7" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i> Reset</a>
                                <a id="tambah_7" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-plus"></i> Tambahkan Ke List</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-1"></div>
                            <div class="col-md-10">
                                <div class="panel panel-default">
                                <table class="table table-bordered table-striped" >
                                    <tr style="background-color: #d5d2d1">
                                        <th class="text-center">Judul</th>
                                        <th class="text-center">Tanggal</th>
                                        <th class="text-center">File Upload (jpg, pdf)</th>
                                        <th class="text-center">Keterangan</th>
                                        <th class="text-center">Aksi</th>
                                    </tr>
                                    <tbody id="tampilDetail_7"></tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <input type="hidden" name="id" value="<?=$id?>">
                    <div class="form-group">
                        <div class="col-md-12">&nbsp;</div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="box">
                            <div class="col-md-2"></div>
                            <div class="col-md-2" align="center">
                                <a href="<?=base_url()?>MsAnggota" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-chevron-left"></i> kembali</a>
                                <button type="submit" class="btn btn-sm btn-success"><i class="glyphicon glyphicon-hdd"></i> <?=$button;?></button>
                            </div>
                        </div>
                    </div>
                </div>                    
            </div>
            </form>
        </div>
        </div>
    </div>
    </div>
    </div>
</section>
<style type="text/css">
.chosen-container { width: 100% !important; }
</style>

<script type="text/javascript">
$(document).ready(function() {
    $('.example_data').dataTable({
        "searching": false,
        "bLengthChange": false,
        "bSort": false,
        'oLanguage': {
            "sProcessing":   "Sedang memproses...",
            "sLengthMenu":   "Tampilkan _MENU_ entri",
            "sZeroRecords":  "Tidak ditemukan data yang sesuai",
            "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
            "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
            "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
            "sInfoPostFix":  "",
            "sSearch":       "Cari:",
            "sUrl":          "",
            "oPaginate": {
                "sFirst":    "<<",
                "sPrevious": "<",
                "sNext":     ">",
                "sLast":     ">>"
            }
        },
    });

    $("#reset_password").click(function(){
        if ($('#reset_password').is(':checked')) {
            $('#info_pswd').show();
        }else{
            $('#info_pswd').hide();
        }
    });

});

$("#tambah_1").click(function(){
    nama_kel = $('#nama_kel').val();
    nik_kel = $('#nik_kel').val();
    hubungan = $('#hub_kel').val();
    tempat_lahir_kel = $('#tempat_lahir_kel').val();
    tgl_lahir_kel = $('#tgl_lahir_kel').val();
    pddkn = $('#pddkn').val();
    status_anak = $('#status_anak').val();
    $("#tampilDetail_1").append(
        '<tr>'+
            '<td class="text-center">'+nama_kel+'</td>'+
            '<td class="text-center">'+nik_kel+'</td>'+
            '<td class="text-center">'+hubungan+'</td>'+
            '<td class="text-center">'+tempat_lahir_kel+'</td>'+
            '<td class="text-center">'+tgl_lahir_kel+'</td>'+
            '<td class="text-center">'+pddkn+'</td>'+
            '<td class="text-center">'+status_anak+'</td>'+
            '<td class="text-center"><a style="cursor: pointer;" title="hapus" class="remove btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>'+
                '<input type="hidden" name="data_keluarga[nama_keluarga][]" value="'+nama_kel+'">'+
                '<input type="hidden" name="data_keluarga[nik_keluarga][]" value="'+nik_kel+'">'+
                '<input type="hidden" name="data_keluarga[hubungan][]" value="'+hubungan+'">'+
                '<input type="hidden" name="data_keluarga[tempat_lahir_kel][]" value="'+tempat_lahir_kel+'">'+
                '<input type="hidden" name="data_keluarga[tgl_lahir_kel][]" value="'+tgl_lahir_kel+'">'+
                '<input type="hidden" name="data_keluarga[pendidikan][]" value="'+pddkn+'">'+
                '<input type="hidden" name="data_keluarga[status_anak][]" value="'+status_anak+'">'+
            '</td>'+
        '</tr>'
    );

    kosong_1();
});

$("#reset_1").click(function(){
    kosong_1();
});
function kosong_1(){
    $('.kosong1').val('');
    $('#hub_kel').trigger("chosen:updated");
    $('#pddkn').trigger("chosen:updated");
    $('#status_anak').trigger("chosen:updated");
    $( "#tgl_lahir_kel" ).datepicker('setDate','');
}
$("#tampilDetail_1").on('click','.remove',function(){
    if(confirm('Apakah anda yakin?')){
        $(this).parent().parent().remove();
    }
});

$("#tambah_2").click(function(){
    rw_tingkat = $('#rw_tingkat').val();
    rw_nama_pddk = $('#rw_nama_pddk').val();
    rw_jurusan = $('#rw_jurusan').val();
    rw_tahun_lulus = $('#rw_tahun_lulus').val();
    rw_tempat_pddk = $('#rw_tempat_pddk').val();
    $("#tampilDetail_2").append(
        '<tr>'+
            '<td class="text-center">'+rw_tingkat+'</td>'+
            '<td class="text-center">'+rw_nama_pddk+'</td>'+
            '<td class="text-center">'+rw_jurusan+'</td>'+
            '<td class="text-center">'+rw_tahun_lulus+'</td>'+
            '<td class="text-center">'+rw_tempat_pddk+'</td>'+
            '<td class="text-center"><a style="cursor: pointer;" title="hapus" class="remove btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>'+
                '<input type="hidden" name="riwayat_pendidikan[tingkat][]" value="'+rw_tingkat+'">'+
                '<input type="hidden" name="riwayat_pendidikan[nama_pddk][]" value="'+rw_nama_pddk+'">'+
                '<input type="hidden" name="riwayat_pendidikan[jurusan][]" value="'+rw_jurusan+'">'+
                '<input type="hidden" name="riwayat_pendidikan[tahun_lulus][]" value="'+rw_tahun_lulus+'">'+
                '<input type="hidden" name="riwayat_pendidikan[tempat_pddk][]" value="'+rw_tempat_pddk+'">'+
            '</td>'+
        '</tr>'
    );

    kosong_2();
});

$("#reset_2").click(function(){
    kosong_2();
});
function kosong_2(){
    $('.kosong2').val('');
    $('#rw_tingkat').trigger("chosen:updated");
}
$("#tampilDetail_2").on('click','.remove',function(){
    if(confirm('Apakah anda yakin?')){
        $(this).parent().parent().remove();
    }
});

$("#tambah_3").click(function(){
    krj_jenis = $('#krj_jenis').val();
    krj_instansi = $('#krj_instansi').val();
    krj_lama_kerja = $('#krj_lama_kerja').val();
    $("#tampilDetail_3").append(
        '<tr>'+
            '<td class="text-center">'+krj_jenis+'</td>'+
            '<td class="text-center">'+krj_instansi+'</td>'+
            '<td class="text-center">'+krj_lama_kerja+'</td>'+
            '<td class="text-center"><a style="cursor: pointer;" title="hapus" class="remove btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>'+
                '<input type="hidden" name="riwayat_pekerjaan[jenis_pekerjaan][]" value="'+krj_jenis+'">'+
                '<input type="hidden" name="riwayat_pekerjaan[nama_instansi][]" value="'+krj_instansi+'">'+
                '<input type="hidden" name="riwayat_pekerjaan[lama_bekerja][]" value="'+krj_lama_kerja+'">'+
            '</td>'+
        '</tr>'
    );

    kosong_3();
});

$("#reset_3").click(function(){
    kosong_3();
});
function kosong_3(){
    $('.kosong3').val('');
}
$("#tampilDetail_3").on('click','.remove',function(){
    if(confirm('Apakah anda yakin?')){
        $(this).parent().parent().remove();
    }
});

$("#tambah_4").click(function(){
    namaJab = $('#jbt_nama_jabatan').val().split('|');
    jbt_tgl_mulai = $('#jbt_tgl_mulai').val();
    pilih_tgl_selesai = $('#pilih_tgl_selesai').val();
    if(pilih_tgl_selesai=='Sekarang'){
        slsai = 'Sekarang';
    }else{
        slsai = $('#jbt_tgl_selesai').val();
    }
    jbt_tgl_selesai = slsai;
    jbt_keterangan = $('#jbt_keterangan').val();
    $("#tampilDetail_4").append(
        '<tr>'+
            '<td class="text-center">'+namaJab[1]+'</td>'+
            '<td class="text-center">'+jbt_tgl_mulai+'</td>'+
            '<td class="text-center">'+jbt_tgl_selesai+'</td>'+
            '<td class="text-center">'+jbt_keterangan+'</td>'+
            '<td class="text-center"><a style="cursor: pointer;" title="hapus" class="remove btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>'+
                '<input type="hidden" name="riwayat_jabatan[fk_jabatan_id][]" value="'+namaJab[0]+'">'+
                '<input type="hidden" name="riwayat_jabatan[rw_nama_jabatan][]" value="'+namaJab[1]+'">'+
                '<input type="hidden" name="riwayat_jabatan[tgl_mulai][]" value="'+jbt_tgl_mulai+'">'+
                '<input type="hidden" name="riwayat_jabatan[tgl_selesai][]" value="'+jbt_tgl_selesai+'">'+
                '<input type="hidden" name="riwayat_jabatan[keterangan_jabatan][]" value="'+jbt_keterangan+'">'+
            '</td>'+
        '</tr>'
    );

    kosong_4();
});

$("#reset_4").click(function(){
    kosong_4();
});
function kosong_4(){
    $('.kosong4').val('');
    $('#jbt_nama_jabatan').trigger("chosen:updated");
    $( "#jbt_tgl_mulai" ).datetimepicker('setDate','');
}
$("#tampilDetail_4").on('click','.remove',function(){
    if(confirm('Apakah anda yakin?')){
        $(this).parent().parent().remove();
    }
});
$("#pilih_tgl_selesai").change(function(){
    $("#tgl_slsi").hide();
    if($(this).val()=='tgl'){
        $("#tgl_slsi").show();
    }
});

$("#tambah_5").click(function(){
    kes_jenis_sakit = $('#kes_jenis_sakit').val();
    kes_tgl_sakit = $('#kes_tgl_sakit').val();
    kes_tempat_perawatan = $('#kes_tempat_perawatan').val();
    kes_lama_perawatan = $('#kes_lama_perawatan').val();
    kes_dokter_yg_menangani = $('#kes_dokter_yg_menangani').val();
    kes_keterangan = $('#kes_keterangan').val();
    $("#tampilDetail_5").append(
        '<tr>'+
            '<td class="text-center">'+kes_jenis_sakit+'</td>'+
            '<td class="text-center">'+kes_tgl_sakit+'</td>'+
            '<td class="text-center">'+kes_tempat_perawatan+'</td>'+
            '<td class="text-center">'+kes_lama_perawatan+'</td>'+
            '<td class="text-center">'+kes_dokter_yg_menangani+'</td>'+
            '<td class="text-center">'+kes_keterangan+'</td>'+
            '<td class="text-center"><a style="cursor: pointer;" title="hapus" class="remove btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>'+
                '<input type="hidden" name="riwayat_kesehatan[jenis_sakit][]" value="'+kes_jenis_sakit+'">'+
                '<input type="hidden" name="riwayat_kesehatan[tgl_sakit][]" value="'+kes_tgl_sakit+'">'+
                '<input type="hidden" name="riwayat_kesehatan[tempat_perawatan][]" value="'+kes_tempat_perawatan+'">'+
                '<input type="hidden" name="riwayat_kesehatan[lama_perawatan][]" value="'+kes_lama_perawatan+'">'+
                '<input type="hidden" name="riwayat_kesehatan[dokter_yg_menangani][]" value="'+kes_dokter_yg_menangani+'">'+
                '<input type="hidden" name="riwayat_kesehatan[keterangan][]" value="'+kes_keterangan+'">'+
            '</td>'+
        '</tr>'
    );

    kosong_5();
});

$("#reset_5").click(function(){
    kosong_5();
});
function kosong_5(){
    $('.kosong5').val('');
    $( "#kes_tgl_sakit" ).datepicker('setDate','');
}
$("#tampilDetail_5").on('click','.remove',function(){
    if(confirm('Apakah anda yakin?')){
        $(this).parent().parent().remove();
    }
});

$("#tambah_7").click(function(){
    dok_judul = $('#dok_judul').val();
    dok_tgl = $('#dok_tgl').val();
    dok_keterangan = $('#dok_keterangan').val();
    $("#tampilDetail_7").append(
        '<tr>'+
            '<td class="text-center">'+dok_judul+'</td>'+
            '<td class="text-center">'+dok_tgl+'</td>'+
            '<td class="text-center"><input type="file" name="file_dokumentasi[]"></td>'+
            '<td class="text-center">'+dok_keterangan+'</td>'+
            '<td class="text-center"><a style="cursor: pointer;" title="hapus" class="remove btn btn-xs btn-danger" ><i class="glyphicon glyphicon-trash"></i></a>'+
                '<input type="hidden" name="dokumentasi[judul][]" value="'+dok_judul+'">'+
                '<input type="hidden" name="dokumentasi[tgl_dokumentasi][]" value="'+dok_tgl+'">'+
                '<input type="hidden" name="dokumentasi[keterangan_dokumentasi][]" value="'+dok_keterangan+'">'+
            '</td>'+
        '</tr>'
    );

    kosong_7();
});

$("#reset_7").click(function(){
    kosong_7();
});
function kosong_7(){
    $('.kosong7').val('');
    $( "#kes_tgl_sakit" ).datepicker('setDate','');
}
$("#tampilDetail_7").on('click','.remove',function(){
    if(confirm('Apakah anda yakin?')){
        $(this).parent().parent().remove();
    }
});
</script>