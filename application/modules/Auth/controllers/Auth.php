<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	function __construct(){
		parent::__construct();

	}

	public function index(){
		$data['aa'] = array();
		$this->load->view('login_new',$data);
	}

	public function prosesLogin(){
		$user = $this->input->post('username');
		$pass = md5($this->input->post('password'));

		$que = "select * from ms_user where blokir='N' and username='$user' and password='$pass' ";
		$hasil = $this->db->query($que)->row();
		if(isset($hasil)){
			$datetime = date('Y-m-d H:i:s');
			$this->db->query("update ms_user set last_login='$datetime' where id=".$hasil->id);
			$this->db->query("insert ms_user_history(fk_user_id,ipaddress,time_act) values(".$hasil->id.",'".$this->input->ip_address()."','".$datetime."') ");

			$data['id'] = $hasil->id;
			$data['fk_anggota_id'] = $hasil->fk_anggota_id;
			$data['username'] = $hasil->username;
			$data['password'] = $hasil->password;
			$data['nama_lengkap'] = $hasil->nama_lengkap;
			$data['level'] = $hasil->level;

			$this->session->set_userdata($data);

			redirect('Home');
		}else{
			$this->session->set_flashdata('error', 'username atau password anda salah.');
            redirect('Auth');
		}
	}

	function logout(){
        $this->session->sess_destroy();
        redirect('Auth');
    }
}
